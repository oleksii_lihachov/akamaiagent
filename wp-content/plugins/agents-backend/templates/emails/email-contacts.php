<?php
    $email_template = get_posts([
        'post_type'   => 'system-email',
        'numberposts' => 1,
        'meta_query'  => array(
			array(
				'key'   => 'system_email_type',
				'value' => 'email-signature'
            )
        )
    ]);
?>

<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin: auto;margin-bottom:20px;max-width: 600px;width: 100%;">
	<tbody>
		<tr>
			<td>
				<?php
                    if( is_array( $email_template ) && count( $email_template ) > 0 ) {
                        echo wpautop($email_template[0]->post_content);
                    }
				?>
			</td>
		</tr>
	</tbody>
</table>