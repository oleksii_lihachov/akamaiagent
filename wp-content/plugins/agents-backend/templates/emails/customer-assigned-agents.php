<?php

$request_id = $params['customer_request_id'];

$first_name = get_field( 'customer_request_customer_first_name', $request_id );
$last_name =  get_field( 'customer_request_customer_last_name', $request_id );

$type = get_field( 'customer_request_customer_type', $request_id );

?>

<table role="presentation" cellpadding="0" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
	<tr>
		<td>
			<p>Congratulations, <?php echo $params['first_name']; ?>!</p>
			<p>You are one step closer to connecting with one of Hawaii’s very best agents to assist you with your real estate needs!</p>
			<p>What next? </p>
			<table role="presentation" cellpadding="0" border="0" cellpadding="0" cellspacing="0" width="100%" class="list">
                <tr>
                    <td><span style="padding-right: 10px">-</span></td>
					<td>Compare the Agent Profiles below.</td>
                </tr>
                <tr>
                    <td><span style="padding-right: 10px">-</span></td>
					<td>Feel free to contact the Agents with any questions you may have.</td>
                </tr>
                <tr>
                    <td><span style="padding-right: 10px">-</span></td>
					<td>Choose an Agent you’d like to work with.</td>
                </tr>
                <tr>
                    <td><span style="padding-right: 10px">-</span></td>
					<td>Let us know which Agent you decide on.</td>
                </tr>
                <tr>
                    <td><span style="padding-right: 10px">-</span></td>
					<td>Be sure to save this email for future reference.</td>
                </tr>
            </table>
			<p>We are always here to help should you have any questions or comments throughout the process – and even afterwards. Feel free to contact us <a href="http://akamaiagent.com/contact-us/">here</a>.<br>
				(Please note our hours of operation below).</p>
			<p>Wishing you all the success in the world with your real estate goals! Thank you for allowing us to connect you with one of Hawaii’s most Akamai Agents!</p>

			<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/customer-assigned-agents-table.php'; ?>

			<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/customer-request-table.php'; ?>
		</td>
	</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>