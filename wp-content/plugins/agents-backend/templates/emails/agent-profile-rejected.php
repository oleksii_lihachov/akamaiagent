<?php

$meta_query = array(
	array(
		'key'   => 'system_email_type',
		'value' => 'application-rejected',
	),
);

$email_template = get_posts(
	array(
		'post_type'   => 'system-email',
		'numberposts' => 1,
		'meta_query'  => $meta_query,
	)
);

?>
<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
			<?php
			if ( is_array( $email_template ) && count( $email_template ) > 0 ) {
				echo str_replace( '{first_name}', $params['first_name'], wpautop( $email_template[0]->post_content ) );
			}
			?>
		</td>
		</tr>
	</tbody>
</table>

<?php require plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>
