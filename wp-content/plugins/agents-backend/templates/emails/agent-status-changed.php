<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<?php if ( $params['status'] === 'active' ): ?>
					
					<p>Congratulations, <?php echo $params['first_name']; ?>!</p>
	 
					<p>Your account is now on “Active status”, which means you will start being referred to appropriate  customers! We’ll be sure to let you know when we refer your profile to a customer. </p>

                    <p>Feel free to <a href="https://akamaiagent.com/contact-us/">contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our  Akamai Agents!
					(Please note our hours of operation below). 
					</p>

				<?php else : ?>

					<p>Congratulations, <?php echo $params['first_name']; ?>!</p>

					<p>Your account is now on “Inactive status”, which means you will not be referred to customers until  your account is Activated. </p>

					<p>Note: This is usually caused by not having all the Required Fields (*) completed in your Agent Profile  (including your agent photo). If you haven’t edited your agent profile page recently, and if all  Required Fields are completely filled in, and your account status is still Inactive, feel free to <a href="https://akamaiagent.com/contact-us/">contact us</a> and we’ll be happy to look into your account status for you.
					(please note our hours of operation below). </p>

				<?php endif; ?>
			</td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>