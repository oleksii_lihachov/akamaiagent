<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<p>Congratulations, <?php echo $params['first_name']; ?>!</p>

				<p>Now that you have completed setting up your profile page, you will start being referred to appropriate customers! We’ll be sure to let you know when that happens. </p>

				<p>Feel free to <a href="http://akamaiagent.com/contact-us/">Contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our Akamai Agents! (Please note our hours of operation) </p>
			</td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>