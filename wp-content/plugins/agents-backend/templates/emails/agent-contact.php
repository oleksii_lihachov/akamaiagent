<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
                <p>Hi <?php echo $params['agent_first_name']; ?>,</p>
                <p><?php echo $params['from_name'] . ' ' . $params['from_last_name']; ?> has sent you a message via your AkamaiAgent Profile  page. Please see their message below</p>
				<p><?php echo $params['message']; ?></p>
			</td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>