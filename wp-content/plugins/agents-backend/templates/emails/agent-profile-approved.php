

<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<p>Congratulations, <?php echo $params['first_name']; ?>! You have been approved to become an Akamai Agent!</p>
				<p>Welcome to our network of Hawaii’s very best agents! We look forward to making referrals to you soon!</p>
				<p>What’s next?</p>
				<ol>
					<li>I will email you a referral agreement shortly, to be signed electronically by you (and your Broker in Charge if applicable).</li>
					<li>Once we have a signed referral agreement, I will email you the information to register your account and create your profile (very easy to do).</li>
					<li>Then just sit back and start receiving referrals! It’s that easy!</li>
				</ol>
				<p>If you have any questions, please see the <a href="http://akamaiagent.com/agent-faq/">Agent FAQ</a> section, give me a call, or simply respond to this email.</p>
			</td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>