<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>akamaiagent.com</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body style="margin: 0; padding: 0;">
 <table class="table-data" cellpadding="0" cellspacing="0" width="100%" style="margin: auto; max-width: 600px;">
	
	<tr>
		<td><h3><a href="<?php echo $data['application_url']; ?>">View</a> application.</h3></td>
	</tr>

 	<?php if ( ! empty( $params) ): ?>
	 	<?php foreach ( $params as $key => $value ): ?>
	 		<tr>
			   	<td>
			    	<?php echo $key; ?>
			  	</td>
			  	<td>
			    	<?php echo $value; ?>
			  	</td>
			 </tr>
	 	<?php endforeach ?>
	<?php endif ?>

 </table>
</body>

</html>