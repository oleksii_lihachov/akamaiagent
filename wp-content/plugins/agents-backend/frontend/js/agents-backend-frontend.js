(function ($) {
  'use strict';


  $(document).ready(function () {

    // Terms Page
    $('#accept-terms').click(function(event) {
      event.preventDefault();

      $.post(
        ajax.ajax_url,
        {
          action: 'agent_accept_terms',
        },
        function(data, textStatus, xhr) {
          console.log(data);
          if ( data.success ) {
            document.location.href = '/';
          }

        }
      );
    });


    // Testimonials
    $(document).ready(function(){
      $('.testimonials').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev"><</button>',
        nextArrow: '<button type="button" class="slick-next">></button>',
        responsive: [
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
    }).on('setPosition', function (event, slick) {
        slick.$slides.css('height', slick.$slideTrack.height() + 'px');
    });

    // Agents Search Tool
    var agentSearchTool = {
      agentId: 0,
      popup: '',
      init() {

        let _self = this;

        _self.popup = $('.agents-search-tool #delete-agent-popup');

        $('.agents-search-tool .remove').click(function(event) {
          event.preventDefault();

          _self.agentId = $(this).data('agent-id');
          _self.popup.find('.agent-id-popup').text( _self.agentId );
          _self.popup.show();
        });

        $('.agents-search-tool #delete-agent-popup #yes').click(function(event) {
          event.preventDefault();
          _self.deleteAgent();
        });

        $('.agents-search-tool #delete-agent-popup #no').click(function(event) {
          event.preventDefault();
          _self.popup.hide();
        });

        $('.agents-search-tool #delete-agent-popup .close').click(function(event) {
          event.preventDefault();
          _self.popup.hide();
        });

        $(document).on( 'click', '.agents-search-tool #clear, .request .search-form #clear', function(event) {
          event.preventDefault();
          window.location = window.location.href.split("?")[0];
        });
      },
      deleteAgent() {
        $.post(
          ajax.ajax_url,
          {
            action: 'delete_agent',
            agent_id: this.agentId,
            nonce: ajax.ajax_nonce
          },
          (data, textStatus, xhr) => {

            if( data.success ) {
              this.popup.hide();
              $('.agents-search-tool tr[data-agent-id="' + this.agentId + '"]').remove();
            } else {
              this.popup.hide();
            }
          }
        );
      }
    }
    agentSearchTool.init();

    // Agent Profile
    var agent = {
      agentId: 0,
      agentName: '',
      customerRequestId: 0,
      tooltip: {
        yelp: 'You can find the Yelp Business ID in the URL for the listing after clicking "Write a Review." The <strong>ID</strong> appears after www.yelp.com/writeareview/biz/ in the address bar, and is generally composed of letters, numbers and other special characters. Ensure your ID ends at the ? at the end of the ID. See example below: <img src="https://downloads.intercomcdn.com/i/o/72287530/6d405e1e3f555545d58da424/image.png">',
        zillow: 'You need to add email which is linked to your Zillow account'
      },
      init() {
        let _self = this;

        let popup = $('#show-emails-popup');

        $('#contact-agent').click(function(event) {
          event.preventDefault();

          _self.agentId = $(this).data('agent-id');
          _self.agentName = $(this).data('agent-name');
          _self.customerRequestId = $(this).data('customer-request-id');

            popup.show();
        });

        $('#show-emails-popup #yes').click(function(event) {
            event.preventDefault();
            _self.showEmailForm();
        });

        $('#show-emails-popup form').submit(function(event) {
          event.preventDefault();

          if ( '' === $('#show-emails-popup form textarea').val() ) {
            $('#show-emails-popup form textarea').addClass('error');
            return false;
          }

          _self.contactAgent();
        });

        $('#show-emails-popup #no').click(function(event) {
            event.preventDefault();
            popup.hide();
        });

        $('#show-emails-popup .close').click(function(event) {
          event.preventDefault();
          popup.hide();
        });

        // Add Tooltip
        _self.addTooltips();

        $('.agent-profile #edit-profile .tooltip').hover(function() {
          _self.toggleTooltip( $(this) );
        }, function() {
          _self.toggleTooltip( $(this) );
        });
      },
      showEmailForm() {
        $('#show-emails-popup .buttons').hide();
        $('#show-emails-popup form').show();
        $('#show-emails-popup h4').text( 'Write your email message to ' + this.agentName + ' below.' );
      },
      contactAgent() {
        $.post(
          ajax.ajax_url,
          {
            action: 'contact_agent',
            agent_id: this.agentId,
            customer_request_id: this.customerRequestId,
            message: $('#show-emails-popup form textarea').val()
          },
          function(data, textStatus, xhr) {
            $('#show-emails-popup form').hide();
            $('#show-emails-popup h4').text( 'Thank you! Your message has been sent.' );
          }
        );
      },
      addTooltips(){
        $('.acf-field-5cd1f3350461a label').append('<span class="tooltip">?</span>');
        $('.acf-field-5cd1f3350461a label').append('<span class="tooltip-text"></span>');
        $('.acf-field-5cd1f3350461a .tooltip-text').html( this.tooltip.yelp );

        $('.acf-field-5cd1f3450461b label').append('<span class="tooltip">?</span>');
        $('.acf-field-5cd1f3450461b label').append('<span class="tooltip-text"></span>');
        $('.acf-field-5cd1f3450461b .tooltip-text').html( this.tooltip.zillow );
      },
      toggleTooltip( el ) {
        el.next().toggleClass('show');
      }
    }
    agent.init();

    // Agent Account
    var agentAccount = {
      agentId: 0,
      popup: '',
      init() {
        let _self = this;

        _self.agentId = $('.edit-account #user_id').val();
        _self.popup   = $('.edit-account #delete-agent-popup');

        $('.edit-account #deleteuser').click(function(event) {
          event.preventDefault();
          _self.popup.show();
        });

        $('.edit-account #delete-agent-popup #yes').click(function(event) {
          event.preventDefault();
          _self.deleteAgent();
        });

        $('.edit-account #delete-agent-popup #no').click(function(event) {
          event.preventDefault();
          _self.popup.hide();
        });

        $('.edit-account #delete-agent-popup .close').click(function(event) {
          event.preventDefault();
          _self.popup.hide();
        });
      },
      deleteAgent() {
        $.post(
          ajax.ajax_url,
          {
            action: 'delete_agent',
            agent_id: this.agentId,
            nonce: ajax.ajax_nonce
          },
          function(data, textStatus, xhr) {

            if( data.success ) {
              window.location.href = '/';
            } else {
              this.popup.hide();
            }
          }
        );
      }
    }
    agentAccount.init();

    // Requests Loop
    var requestsLoop = {
      init() {

        let _self = this;

        $(document).on( 'change', '.requests select.request-status', function(event) {
          let el = $(this);
          let request_id = el.data( 'request-id');
          let newStatus = el.val();

          _self.changeStatus( newStatus, request_id );

        });

        $(document).on( 'click', '.requests #clear', function(event) {
          event.preventDefault();
          window.location = window.location.href.split("?")[0];
        });
      },
      changeStatus( newStatus, request_id ) {
        $.post(
          ajax.ajax_url,
          {
            action: 'change_request_status',
            request_id: request_id,
            new_status: newStatus
          },
          function(data, textStatus, xhr) {
          
            if ( data.success ) {
              let result = $('#request-' + request_id + ' .request-status-col .result' );
              result.text( data.data.message );

              setTimeout( () => {
                result.text( '' );
              }, 6000 );
            }
          }
        );
      }
    }
    requestsLoop.init();


    // Single Request
    var request = {
      init: function() {

        let _self = this;

        $(document).on( 'click', '.request .agents-search h2', function(event) {
          $('.request .agents-search h2').toggleClass('opened');
          $('.request .agents-search .search-form').toggleClass('show');
        });

        $(document).on( 'click', '.request .detailed-profile', function(event) {
          _self.get_detailed_profile( $(this).data('agent-id') );
        });

        $(document).on( 'click', '.request button.select-agent', function(event) {
          _self.add_agent_to_request( $(this).data('agent-id'), $(this).closest('.request').data('request-id') );
        });

        $(document).on( 'click', '.request button.unselect-agent', function(event) {
          _self.remove_agent_from_request( $(this).data('agent-id'), $(this).closest('.request').data('request-id') );
        });

        $(document).on( 'submit', '.request .search-form form', function(event) {
          event.preventDefault();

          let data = $(this).serializeArray();
          let result = {};
          $.each( data, function() {
            if (typeof ( result[this.name] ) === "undefined") {
              result[this.name] = this.value;
            }
            else {
              result[this.name] += "," + this.value;
            }
          });

          _self.search_agents( $(this).data('request-id'), result );
        });

        $(document).on( 'click', '.request #send_to_customer', function(event) {
          _self.send_agents_to_customer( $(this).data('request-id') );
        });

      },
      get_detailed_profile: function( agent_id ) {
        $.post(
          ajax.ajax_url,
          {
            action: 'get_datailed_profile',
            agent_id: agent_id
          },
          function(data, textStatus, xhr) {
            $('#detailed-content').html(data);
          }
        );
      },
      add_agent_to_request: function( agent_id, request_id ){
        $.post(
          ajax.ajax_url,
          {
            action: 'add_agent_to_request',
            agent_id: agent_id,
            request_id: request_id
          },
          function(data, textStatus, xhr) {
            if ( data.success ) {
              $('button.select-agent[data-agent-id="' + agent_id + '"]').text('Selected').addClass('success unselect-agent').removeClass('primary select-agent');

              request.render_selected_agents( request_id );
            }
          }
        );
      },
      remove_agent_from_request: function( agent_id, request_id ){
        $.post(
          ajax.ajax_url,
          {
            action: 'remove_agent_from_request',
            agent_id: agent_id,
            request_id: request_id
          },
          function(data, textStatus, xhr) {
            if ( data.success ) {
              $('button.unselect-agent[data-agent-id="' + agent_id + '"]').text('Select').removeClass('success unselect-agent').addClass('primary select-agent');

              request.render_selected_agents( request_id );
            }
          }
        );
      },
      render_selected_agents: function( request_id ) {
        $.post(
          ajax.ajax_url,
          {
            action: 'render_selected_agents',
            request_id: request_id
          },
          function(data, textStatus, xhr) {
            if ( data.success ) {
              $('#selected-agents').html(data.data);
            }
          }
        );
      },
      search_agents: function( request_id, data ) {

        $.post(
          ajax.ajax_url,
          {
            action: 'search_agents',
            request_id: request_id,
            data: data
          },
          function(data, textStatus, xhr) {
            console.log(data);
            if ( data.success ) {
              $('.available-agents').html(data.data.html);
            }
          }
        );
      },
      send_agents_to_customer: function( request_id ){
          $.post(
              ajax.ajax_url,
              {
                  action: 'send_agents_to_customer',
                  request_id: request_id
              },
              function(data, textStatus, xhr) {
                  if(data.success){
                      $('#send_to_customer').addClass('success').text( 'Email has been sent' ).prop('disabled', true);
                  }
              }
          );
      }
    };
    request.init();


    // Emails Center
    var emailCenter = {
      init(){

        const _self = this;

        $('.email-center #user-email').select2();

        $('.email-center input[name="recipient"]').click(function(event) {
          if( $(this).val() === 'user' ){
            $('.email-center .recipients .select2').css( 'display', 'block' );
          } else {
            $('.email-center .recipients .select2').hide();
          }
        });

        $('.email-center .templates span').click(function(event) {
          const template_id = $(this).data('id');
          _self.setTemplate( template_id );
        });
      },
      setTemplate( template_id ) {

        $.post(
          ajax.ajax_url,
          {
            action: 'set_email_template',
            template_id: template_id
          },
          function(data, textStatus, xhr) {
            if ( data.success ) {
              let message = data.data;
              message = message.replace(/\n/g, '<br/>');
              
              tinymce.editors.admin_message.setContent(message);
            }
          }
        );
      }
    }
    emailCenter.init();


    // Popups Notification
    $('.agents-popup a.close').click(function(event) {
      event.preventDefault();
      //$('.agents-popup, .agents-popup-overlay').hide();
      document.location.href = '/';
    });

  });

})(jQuery);
