<h1>New Applications</h1>

<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>Agent Name</th>
			<th>Company name</th>
			<th colspan="2">Actions</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ( $applications as $key => $application ): ?>
			<tr>
				<td><?php echo $application->ID ?></td>
				<td><?php echo get_field( 'agent_first_name', $application->ID ) . ' ' . get_field( 'agent_last_name', $application->ID ); ?></td>
				<td><?php echo get_field( 'agent_company_name', $application->ID ); ?></td>
				<td><a href="?action=view&application_id=<?php echo $application->ID; ?>">View</a></td>
				<td></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>