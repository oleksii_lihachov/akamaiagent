<div class="email-center">

	<div class="heading">
		<h1>Email Center</h1>

		<div class="button-wrap inbox">
			<a class="btn primary" href="https://secure257.inmotionhosting.com:2096/" target="_blank">Email Inbox</a>
		</div>
	</div>

	<?php if ( isset($_GET['status'] ) && isset($_GET['message'] ) ): ?>
		<div class="message">
			<p class="<?php echo $_GET['status']; ?>"><?php echo $_GET['message']; ?></p>
		</div>
	<?php endif ?>

	<h2>Send Message to users</h2>
	<form action="" method="post">
		<div class="form-group mb-4 recipients">
			<h3>Recipients</h3>
			<label for="everyone"><input type="radio" id="everyone" name="recipient" value="everyone">Everyone</label>
			<label for="agents"><input type="radio" id="agents" name="recipient" value="agents">All Agents</label>
			<label for="customers"><input type="radio" id="customers" name="recipient" value="customers">All Customers</label>
			<label for="user"><input type="radio" id="user" name="recipient" value="user">Agent/Customer</label>

			<select class="form-control" id="user-email" name="user-email" placeholder="Enter name/email">
				<?php foreach ( $recipients as $email => $name ): ?>
					<option value="<?php echo $email ?>"><?php echo $name . ' (' . $email . ')'; ?></option>
				<?php endforeach ?>
		    </select>
		</div>
		<div class="form-group mb-4">
		    <h3>Subject</h3>
		    <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject field">
		</div>
		<div class="form-group mb-4">
		    <h3>Message</h3>

			<div class="templates">
				<p><strong>Select template (<a href="/wp-admin/edit.php?post_type=template" target="_blank">Manage Templates</a>):</strong></p>
				<?php foreach ( $templates as $key => $template ): ?>
					<span data-id="<?php echo $template->ID ?>"><?php echo $template->post_title; ?></span>
				<?php endforeach ?>
			</div>

		    <?php wp_editor( '', 'admin_message', [ 'media_buttons' => false ] ); ?>
		</div>
		<input type="hidden" name="action" value="admin-message">
		<input type="submit" value="Send Email">
	</form>	
</div>