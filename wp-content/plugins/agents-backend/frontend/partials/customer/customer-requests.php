<div class="requests">
	<?php if ( isset( $result ) && $result ): ?>
		<div class="message alert alert-primary"><?php echo $result ?></div>
	<?php endif ?>

	<h1>New Requests</h1>

	<?php include 'customer-requests-filter.php' ?>

	<table>
		<thead>
			<tr>
				<th width="75px">ID</th>
				<th>Request Date</th>
				<th>Customer Name</th>
				<th>Customer Email</th>
				<th class="address">Address</th>
				<th>Type of Customer / Property</th>
				<th class="price">Price Range</th>
				<th class="action">Request Status</th>
				
				<th>Actions</th>
			</tr>
		</thead>
		<?php while ( $requests->have_posts() ) : ?>
			<?php 
				$requests->the_post();
				$request_id = get_the_ID();

				$request_class = new \Agents_Backend\Request( $request_id );
			?>
			<tr id="request-<?php echo $request_id; ?>">
				<td><?php echo $request_class->request_custom_id; ?></td>
				<td><?php echo get_the_date( 'm/d/Y' ); ?></td>
				<td><?php echo $request_class->customer_first_name . ' ' . $request_class->customer_last_name; ?></td>
				<td>
					<?php echo $request_class->customer_email; ?>
					
					<?php if ( 'yes' === $request_class->is_email_confirmed ): ?>
						<p class="email-status confirmed">Confirmed</p>
					<?php else : ?>
						<p class="email-status not-confirmed">Not Confirmed</p>
					<?php endif; ?>
					
				</td>
				<td><?php echo $request_class->get_request_address_text(); ?></td>
				<td><?php echo $request_class->get_request_type_property_type_text(); ?></td>
				<td><?php echo $request_class->get_request_price_text(); ?></td>
				<td class="request-status-col">
					<select name="request-status" class="request-status" data-request-id="<?php echo $request_id; ?>">
						<?php foreach ( $request_class->available_statuses as $value => $label ): ?>
							<option value="<?php echo $value; ?>" <?php echo ( $label === get_field( 'customer_request_status', $request_id ) ) ? 'selected' : ''; ?>><?php echo $label; ?></option>
						<?php endforeach ?>
					</select>
					<p class="result"></p>
				</td>
				<td class="action"><a href="?action=view&request_id=<?php echo $request_class->request_custom_id; ?>">View</a></td>
			</tr>
		<?php endwhile; ?>
	</table>

	<div class="pagination">
		<ul>
			<li><?php echo get_previous_posts_link('< Prev', $requests->max_num_pages); ?></li>
			<li><?php echo get_next_posts_link('Next >', $requests->max_num_pages); ?></li>
		</ul>
	</div>

	<?php wp_reset_query(); ?>
</div>