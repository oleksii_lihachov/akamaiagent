<div id="customer_request_form">
	<div id="customer_type">
		<h3>I’m planning to…</h3>
		<div data-type="Seller" data-step="Sell">Sell a property</div>
		<div data-type="Buyer" data-step="Buy">Buy a property</div>
		<div data-type="Seller & Buyer" data-step="Sell">Sell & Buy</div>
	</div>

	<div id="questions">
		<div class="customer_request_property_type question d-none" data-step="customer_request_property_type" data-next="customer_request_price_range">
			<h3>What type of property would you like to sell?</h3>
			<div class="fields">
				[radio customer_request_property_type use_label_element "Single Family Home" "Condo/Townhouse" "Vacant Land"]
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_price_range question d-none" data-step="customer_request_price_range" data-next="customer_request_how_soon">
			<h3>Roughly, how much is your property worth?</h3>
			<div class="fields">
				<div class="d-none skip-transform d-xl-block">
					[radio customer_request_price_range "$300K or less" "$301K - $500K" "$501K - $750K" "$751K - $950K" "$951K - $1.5M" "$1.5M – $3M" "$3M and more"]
				</div>
				<div class="d-xl-none">
					[select customer_request_price_range first_as_label "Please select" "$300K or less" "$301K - $500K" "$501K - $750K" "$751K - $950K" "$951K - $1.5M" "$1.5M – $3M" "$3M and more"]
				</div>
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_how_soon question d-none" data-step="customer_request_how_soon" data-next="customer_request_need_language">
			<h3>How soon do you plan to sell?</h3>
			<div class="fields">
				[radio customer_request_how_soon "ASAP" "1 – 2 months" "3 – 6 months" "7 – 12 months" "Over 12 months"]
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_preapproved_by_lender question d-none" data-step="customer_request_preapproved_by_lender" data-next="customer_request_need_language">
			<h3>Have you been preapproved by a lender?</h3>
			<div class="fields">
				[radio customer_request_preapproved_by_lender "Yes" "No"]
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_need_language question d-none" data-step="customer_request_need_language" data-next="customer_request_property_address_seller">
			<h3>Do you need an agent who speaks a language other than English?</h3>
			<div class="fields">
				[radio customer_request_need_language "No" "Yes"]
				[select customer_request_other_language first_as_label class:d-none "Select your preferred language" "Afrikaans" "Albanian" "Arabic" "Armenian" "Basque" "Bengali" "Bulgarian" "Catalan" "Cambodian" "Cantonese" "Chinese (Mandarin)" "Croatian" "Czech" "Danish" "Dutch" "Estonian" "Fiji" "Finnish" "French" "Georgian" "German" "Greek" "Gujarati" "Hebrew" "Hindi" "Hungarian" "Icelandic" "Indonesian" "Irish" "Italian" "Japanese" "Javanese" "Korean" "Latin" "Latvian" "Lithuanian" "Macedonian" "Malay" "Malayalam" "Maltese" "Maori" "Marathi" "Mongolian" "Nepali" "Norwegian" "Persian" "Polish" "Portuguese" "Punjabi" "Quechua" "Romanian" "Russian" "Samoan" "Serbian" "Slovak" "Slovenian" "Spanish" "Swahili" "Swedish" "Tagalog" "Tamil" "Tatar" "Telugu" "Thai" "Tibetan" "Tonga" "Turkish" "Ukrainian" "Urdu" "Uzbek" "Vietnamese" "Welsh" "Xhosa"]
				<a class="btn primary d-none" data-next="customer_request_property_address">Next</a>
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_property_address_seller question d-none" data-step="customer_request_property_address_seller" data-next="customer_request_customer_name">
			<h3>What is the address of the property you are selling?</h3>
			<div class="fields">
				[text customer_request_property_address_street placeholder "Street address"]
				[text customer_request_property_address_unit placeholder "Unit #"]
				[text customer_request_property_address_city placeholder "City"]
				[select customer_request_property_address_state include_blank "Alabama|AL" "Alaska|AK" "Arizona|AZ" "Arkansas|AR" "California|CA" "Colorado|CO" "Connecticut|CT" "Delaware|DE" "Florida|FL" "Georgia|GA" "Hawaii|HI" "Idaho|ID" "Illinois|IL" "Indiana|IN" "Iowa|IA" "Kansas|KS" "Kentucky|KY" "Louisiana|LA" "Maine|ME" "Maryland|MD" "Massachusetts|MA" "Michigan|MI" "Minnesota|MN" "Mississippi|MS" "Missouri|MO" "Montana|MT" "Nebraska|NE" "Nevada|NV" "New Hampshire|NH" "New Jersey|NJ" "New Mexico|NM" "New York|NY" "North Carolina|NC" "North Dakota|ND" "Ohio|OH" "Oklahoma|OK" "Oregon|OR" "Pennsylvania|PA" "Rhode Island|RI" "South Carolina|SC" "South Dakota|SD" "Tennessee|TN" "Texas|TX" "Utah|UT" "Vermont|VT" "Virginia|VA" "Washington|WA" "West Virginia|WV" "Wisconsin|WI" "Wyoming|WY"]
				[text customer_request_property_address_zipcode placeholder "Zip Code"]
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_property_address_buyer question d-none" data-step="customer_request_property_address_buyer" data-next="customer_request_customer_name">
			<h3>Enter the zip code of the area you plan to buy (best estimate).</h3>
			<div class="fields">
				[text customer_request_customer_zip placeholder "Zip Code"]
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_outside_of_hawaii question d-none" data-step="customer_request_outside_of_hawaii" data-next="customer_request_notice">
			<h3>Oops, we noticed you entered an address outside of Hawaii. We currently provide the Akamai Agent service only within the state of Hawaii, although we have contacts and resources across the country. We would be happy to reach out to our trusted contacts and refer you to a top agent in the area you are planning to sell.</h3>
			<div class="fields">
				[radio customer_request_outside_of_hawaii "Yes, please refer me to a top agent outside of Hawaii" "No thanks"]
			</div>
			<a class="btn primary">Next</a>
		</div>
		<div class="customer_request_customer_name question d-none" data-step="customer_request_customer_name" data-next="customer_request_notice">
			<h3>Where should we email your FREE Agent Profiles?</h3>
			<div class="fields">
				<div class="row-two">
					[text* customer_request_customer_first_name placeholder "First Name"]
					[text* customer_request_customer_last_name placeholder "Last Name"]
				</div>
				[email* customer_request_customer_email placeholder "Email"]
				[acceptance terms]Agree to <a class="inline" href="/terms-of-use-customers/" target="_blank">terms of use</a>[/acceptance]
				<div id="recaptcha"></div>
			</div>
			<a class="btn primary">Next</a>
		</div>

		<div class="customer_request_notice question d-none" data-step="customer_request_notice">
			<h3>Great! We sent you a link to confirm your email address. Once your email has been confirmed you will receive your top agent profiles shortly.</h3>
			<div><a class="btn primary" href="/">Home Page</a></div>
		</div>
	</div>

	[hidden customer_request_customer_type]

	[submit class:d-none "Send"]

</div>
<div class="request_form_progress">
	<div data-step="customer_type">I’m planning to<br><span></span></div>
	<div data-step="customer_request_property_type">Property type<br><span></span></div>
	<div data-step="customer_request_price_range">Estimated value<br><span></span></div>
	<div data-step="customer_request_how_soon">Estimated Timeline<br><span></span></div>
	<div data-step="customer_request_need_language">Language<br><span></span></div>
</div>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
		async defer>
</script>