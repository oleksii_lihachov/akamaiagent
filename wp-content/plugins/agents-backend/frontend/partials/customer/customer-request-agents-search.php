<div class="search-form">
	<form action="" method="post" data-request-id="<?php echo $request_id; ?>">
		<div class="fields-wrap two-cols">
			<h4>Type of Customer / Property:</h4>
			<?php if ( $request_class->request_type === 'Seller' ): ?>
				<label for="s_single_family">
					<input 
						type="checkbox" 
						id="s_single_family" 
						name="agent_seller_preferences" 
						value="single_family" 
						<?php echo ( get_field( 'customer_request_seller_customer_request_property_type', $request_id ) === 'Single Family Home' ) ? 'checked' : '' ?>>Seller - Single Family Residence</label>
				<label for="s_condo_townhouse">
					<input 
						type="checkbox" 
						id="s_condo_townhouse" 
						name="agent_seller_preferences" 
						value="condo_townhouse" 
						<?php echo ( get_field( 'customer_request_seller_customer_request_property_type', $request_id ) === 'Condo/Townhouse' ) ? 'checked' : '' ?>>Seller - Condo / Townhouse</label>
				<label for="s_vacant_land">
					<input 
						type="checkbox" 
						id="s_vacant_land" 
						name="agent_seller_preferences" 
						value="vacant_land" 
						<?php echo ( get_field( 'customer_request_seller_customer_request_property_type', $request_id ) === 'Vacant Land' ) ? 'checked' : '' ?>>Seller - Vacant Land</label>
			<?php endif ?>
			
			<?php if ( $request_class->request_type === 'Buyer' ): ?>
				<label for="b_single_family">
					<input 
						type="checkbox" 
						id="b_single_family" 
						name="agent_buyer_preferences" 
						value="single_family" 
						<?php echo ( get_field( 'customer_request_buyer_customer_request_property_type', $request_id ) === 'Single Family Home' ) ? 'checked' : '' ?>>Buyer - Single Family Residence</label>
				<label for="b_condo_townhouse">
					<input 
						type="checkbox" 
						id="b_condo_townhouse" 
						name="agent_buyer_preferences" 
						value="condo_townhouse" 
						<?php echo ( get_field( 'customer_request_buyer_customer_request_property_type', $request_id ) === 'Condo/Townhouse' ) ? 'checked' : '' ?>>Buyer - Condo / Townhouse</label>
				<label for="b_vacant_land">
					<input 
						type="checkbox" 
						id="b_vacant_land" 
						name="agent_buyer_preferences" 
						value="vacant_land" 
						<?php echo ( get_field( 'customer_request_buyer_customer_request_property_type', $request_id ) === 'Vacant Land' ) ? 'checked' : '' ?>>Buyer - Vacant Land</label>
			<?php endif ?>			
		</div>

		<div class="fields-wrap two-cols">
			<h4>Estimated Home Value or Buyer Price Range:</h4>
			<label for="300K">
				<input 
					type="checkbox" 
					id="300K" 
					name="agent_property_value" 
					value="$300K or less" 
					<?php echo ( get_field( 'customer_request_' . strtolower($request_class->request_type) . '_customer_request_price_range', $request_id ) === '$300K or less' ) ? 'checked' : '' ?>>$300K or less</label>
			<label for="301K">
				<input 
					type="checkbox" 
					id="301K" 
					name="agent_property_value" 
					value="$301K - $500K" 
					<?php echo ( get_field( 'customer_request_' . strtolower($request_class->request_type) . '_customer_request_price_range', $request_id ) === '$301K - $500K' ) ? 'checked' : '' ?>>$301K - $500K</label>
			<label for="501K">
				<input 
					type="checkbox" 
					id="501K" 
					name="agent_property_value" 
					value="$501K - $750K" 
					<?php echo ( get_field( 'customer_request_' . strtolower($request_class->request_type) . '_customer_request_price_range', $request_id ) === '$501K - $750K' ) ? 'checked' : '' ?>>$501K - $750K</label>
			<label for="751K">
				<input 
					type="checkbox" 
					id="751K" 
					name="agent_property_value" 
					value="$751K - $950K" 
					<?php echo ( get_field( 'customer_request_' . strtolower($request_class->request_type) . '_customer_request_price_range', $request_id ) === '$751K - $950K' ) ? 'checked' : '' ?>>$751K - $950K</label>
			<label for="951K">
				<input 
					type="checkbox" 
					id="951K" 
					name="agent_property_value" 
					value="$951K - $1.5M" 
					<?php echo ( get_field( 'customer_request_' . strtolower($request_class->request_type) . '_customer_request_price_range', $request_id ) === '$951K - $1.5M' ) ? 'checked' : '' ?>>$951K - $1.5M</label>
			<label for="1.5M">
				<input 
					type="checkbox" 
					id="1.5M" 
					name="agent_property_value" 
					value="$1.5M – $3M" 
					<?php echo ( get_field( 'customer_request_' . strtolower($request_class->request_type) . '_customer_request_price_range', $request_id ) === '$1.5M – $3M' ) ? 'checked' : '' ?>>$1.5M – $3M</label>
			<label for="3M">
				<input 
					type="checkbox" 
					id="3M" 
					name="agent_property_value" 
					value="$3M and more" 
					<?php echo ( get_field( 'customer_request_' . strtolower($request_class->request_type) . '_customer_request_price_range', $request_id ) === '$3M and more' ) ? 'checked' : '' ?>>$3M and more</label>
		</div>

		<div class="fields-wrap one-col">
			<h4>Zip Code of property to Sell, or desired area to Purchase:</h4>
			<input type="text" name="zipcode" value="<?php echo ( get_field( 'customer_request_seller_customer_request_property_address_zipcode', $request_id ) ) ? get_field( 'customer_request_seller_customer_request_property_address_zipcode', $request_id ) : get_field( 'customer_request_buyer_customer_request_customer_zip', $request_id ) ?>">
		</div>

		<div class="fields-wrap one-col">
			<h4>Language:</h4>
			<select name="language" id="language">
				<option value="English">English</option>
				<?php foreach ( agents_backend_get_languages() as $language ): ?>
					<option value="<?php echo $language ?>" <?php echo ( get_field( 'customer_request_need_language', $request_id ) !== 'No' && get_field( 'customer_request_other_language', $request_id ) === $language ) ? 'selected' : ''; ?>><?php echo $language ?></option>
				<?php endforeach ?>
			</select>
		</div>

		<div class="fields-wrap one-col">
			<label>Enter agent name, email or company:</label>
			<input type="text" name="s" placeholder="agent name, email or company...">
		</div>

		<div class="btn-wrap one-col">
			<button class="btn primary">Filter</button>
		</div>
	</form>
</div>