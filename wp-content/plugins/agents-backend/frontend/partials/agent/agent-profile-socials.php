<?php if( is_array( $agent_class->socials ) ): ?>
	<div class="socials">
		<ul>
			<?php foreach ( $agent_class->socials as $key => $value ): ?>
				<?php if ( ! empty( $value ) ): ?>
					<li class="<?php echo $key; ?>">
						<a href="<?php echo $value; ?>">
							<?php if ( 'zillow' !== $key ): ?>
								<i class="fab fa-<?php echo $key; ?>"></i>
							<?php else: ?>
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/zillow.png" alt="Zillow" width="21px" style="margin-top: -5px;">
							<?php endif; ?>
						</a>
					</li>
				<?php endif ?>
			<?php endforeach ?>
		</ul>
	</div>
<?php endif; ?>