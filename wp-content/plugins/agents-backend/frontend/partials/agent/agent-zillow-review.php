<div class="zillow-rating">
	<div class="zsg-rating <?php echo $rating_class ?>" data-zillow-rating="<?php echo $zillow_data->proInfo->avgRating; ?>"></div>
	<a href="<?php echo $zillow_data->proInfo->profileURL; ?>"><span><?php echo $zillow_data->proInfo->reviewCount ?> Reviews on Zillow</span></a>
</div>