<div class="edit-account entry-content entry">
    
	<?php if ( isset( $_POST ) && ! empty( $_POST ) ): ?>
		<div class="message">
			<?php if ( $error ): ?>
				<?php echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
			<?php else : ?>
				<p class="success"><?php _e('Account Updated', 'agents-backend'); ?></p>
			<?php endif; ?>
		</div>
	<?php endif ?>

    <?php if ( ! is_user_logged_in() ) : ?>
            <p class="warning">
                <?php _e('You must be logged in to edit your profile.', 'agents-backend'); ?>
            </p><!-- .warning -->
    <?php else : ?>
        <?php 
        	if ( ! $current_user ) {
        		_e('You must be logged in to edit your profile.', 'agents-backend');
        	}
        ?>

        <form method="post" id="adduser" action="<?php the_permalink(); ?>">
        	<p class="form-vacation">
                <label for="vacation-status"><?php _e('Vacation Status', 'agents-backend'); ?></label>
                <select name="vacation-status" id="vacation-status">
                	<option value="no" <?php echo ( 'no' === get_field( 'agent_vacation_status', 'user_' . $user_class->agent_id ) ) ? 'selected' : ''; ?>>Off</option>
                	<option value="yes" <?php echo ( 'yes' === get_field( 'agent_vacation_status', 'user_' . $user_class->agent_id ) ) ? 'selected' : ''; ?>>On</option>
                </select>
            </p><!-- .form-username -->
            <p class="form-username">
                <label for="first-name"><?php _e('First Name', 'agents-backend'); ?></label>
                <input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
            </p><!-- .form-username -->
            <p class="form-username">
                <label for="last-name"><?php _e('Last Name', 'agents-backend'); ?></label>
                <input class="text-input" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
            </p><!-- .form-username -->
            <p class="form-email">
                <label for="email"><?php _e('E-mail *', 'agents-backend'); ?></label>
                <input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
            </p><!-- .form-email -->
            <p class="form-password">
                <label for="pass1"><?php _e('Password *', 'agents-backend'); ?> </label>
                <input class="text-input" name="pass1" type="password" id="pass1" />
            </p><!-- .form-password -->
            <p class="form-password">
                <label for="pass2"><?php _e('Repeat Password *', 'agents-backend'); ?></label>
                <input class="text-input" name="pass2" type="password" id="pass2" />
            </p><!-- .form-password -->

            <p class="form-submit">
                <input name="updateuser" type="submit" id="updateuser" class="submit button" value="<?php _e('Update', 'agents-backend'); ?>" />
                <input name="deleteuser" type="submit" id="deleteuser" class="submit button delete" value="<?php _e('Delete Account', 'agents-backend'); ?>" />
                <?php wp_nonce_field( 'update_account_nonce', 'update-account-nonce' ) ?>
                <input name="user_id" type="hidden" id="user_id" value="<?php echo $current_user->ID; ?>" />
                <input name="action" type="hidden" id="action" value="update-account" />
            </p><!-- .form-submit -->
        </form><!-- #adduser -->
    <?php endif; ?>

    <div id="delete-agent-popup">
        <div class="popup-wrap">
            <span class="close">X</span>
            <h4>Are you sure you want to Delete your account? Doing so will cause you to permanently loose all your content on this website.</h4>
            <div class="buttons">
                <a href="#" id="no">Cancel</a>
                <a href="#" id="yes">Yes, I want to totally Delete my account from this website/service</a>
            </div>
        </div>
    </div>

</div><!-- .entry-content -->