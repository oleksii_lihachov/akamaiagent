<div class="agent-profile edit container">
    <a href="/agent-profile/" class="btn alt success">Back to Profile</a>

	<?php if ( isset( $_POST ) && ! empty( $_POST ) ): ?>
        <div class="message">
            <p class="success"><?php _e( 'Profile Updated', 'agents-backend' ); ?></p>
        </div>
	<?php endif ?>

    <script>
      function openCity(evt, cityName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].closest('li').className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
      }

      document.addEventListener('DOMContentLoaded', function () {

        document.getElementById("defaultOpen").click();
      });


    </script>
    <div class="acf-fields" id="edit-profile">
        <div class="acf-tab-wrap">
            <ul class="acf-hl acf-tab-group">
                <li id="defaultOpen" class="tablinks" onclick="openCity(event, 'General')">
                    <a href="javascript:void(0);" class="acf-tab-button">General</a>
                </li>
                <li class="tablinks" onclick="openCity(event, 'Company')">
                    <a href="javascript:void(0);" class="acf-tab-button">Company</a>
                </li>
                <li class="tablinks" onclick="openCity(event, 'Experience')">
                    <a href="javascript:void(0);" class="acf-tab-button">Experience</a>
                </li>
                <li class="tablinks" onclick="openCity(event, 'About')">
                    <a href="javascript:void(0);" class="acf-tab-button">About</a>
                </li>
                <li class="tablinks" onclick="openCity(event, 'Preferences')">
                    <a href="javascript:void(0);" class="acf-tab-button">Preferences</a>
                </li>
            </ul>
        </div>

        <div id="General" class="tabcontent">
			<?php
			acf_form(
				array(
					'id'                    => 'edit-profile-general',
					'post_id'               => 'user_' . $agent->ID,
					'fields'                => array(
						// Photo
						'field_5cc994c426f23',
						// First name
						'field_5cc9b80293c11',
						// Last name
						'field_5cc9b81493c12',
						// Specialtles
						'field_5ccc8024d7fa0',
						// Certifications
						'field_5ccc807b25621',
						// Awards
						'field_5cd1f211e994f',
						// License number
						'field_5cc5ca1234024',
					),
					'instruction_placement' => 'label',
					'uploader'              => 'wp',
					'return'                => false
				) );
			?>
        </div>

        <div id="Company" class="tabcontent">
			<?php
			acf_form(
				array(
					'id'                    => 'edit-profile-company',
					'post_id'               => 'user_' . $agent->ID,
					'fields'                => array(
						// Agent Company Name
						'field_5cc5ca6434028',
						// Agent Company Phone
						'field_5cc5ca6834029',
						// Company Address
						'field_5cc5ca703402a',
					),
					'instruction_placement' => 'label',
					'uploader'              => 'wp',
					'return'                => false
				) );
			?>
        </div>

        <div id="Experience" class="tabcontent">
			<?php
			acf_form(
				array(
					'id'                    => 'edit-profile-experience',
					'post_id'               => 'user_' . $agent->ID,
					'fields'                => array(
						// Number of years as a licensed agent in the state of Hawaii?
						'field_5ccc6cb6df211',
						// Number of transactions (sides) in the last 12 months?
						'field_5ccc6cd7df212',
						// Enter your preferred ratings page(s):
						'field_5cc5cb38dad16',
					),
					'instruction_placement' => 'label',
					'uploader'              => 'wp',
					'return'                => false
				) );
			?>
        </div>

        <div id="About" class="tabcontent">
			<?php
			acf_form(
				array(
					'id'                    => 'edit-profile-about',
					'post_id'               => 'user_' . $agent->ID,
					'fields'                => array(
						// Socials
						'field_5cd1f37a0461c',
						// List any languages you are fluent in
						'field_5ccc6bc32e27b',
						// Agent BIO
						'field_5cc99776e66ba',
						// Testimonials
						'field_5ccc702005f1f'
					),
					'instruction_placement' => 'label',
					'uploader'              => 'wp',
					'return'                => false
				) );
			?>
        </div>

        <div id="Preferences" class="tabcontent">
			<?php
			acf_form(
				array(
					'id'                    => 'edit-profile-preferences',
					'post_id'               => 'user_' . $agent->ID,
					'fields'                => array(
						// Enter zip codes here:
						'field_5cd8627e673b1',
						// Enter your “Seller” preferences below:
						'field_5cd8640b1e89b',
						// I want to receive Seller referrals within the following estimated property value ranges:
						'field_5cd864e448db7',
						// Enter your “Buyer” preferences and commission amounts below:
						'field_5cd8654cdfab6',
						// I want to receive Buyer referrals within the following estimated property value ranges:
						'field_5cd86577dfab7',

					),
					'instruction_placement' => 'label',
					'uploader'              => 'wp',
					'return'                => false
				) );
			?>
        </div>
    </div>
</div>