<div class="agents-search-tool">

	<h1>All Agents</h1>

	<?php include 'agents-search-filter.php' ?>

	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Company</th>
				<th>Status</th>
				<th class="zip">Zipcodes</th>
				<th class="preferences">Preferences</th>
				<th class="property-value">Property value</th>
				<th>Language</th>
				<th>Terms Accepted</th>
			</tr>
		</thead>
		<tbody>
			<?php if ( ! empty( $agent_query ) ): ?>
				<?php foreach ( $agent_query as $agent ): ?>
					<?php 
						$agent_class = new \Agents_Backend\Agent( $agent->ID );
					?>
					<tr data-agent-id="<?php echo $agent->ID; ?>">
						<td>
							<a href="/agent-profile/?action=view&agent_id=<?php echo $agent_class->agent_id; ?>"><strong><?php echo $agent_class->first_name; ?> <?php echo $agent_class->last_name; ?></strong></a>

							<div>
								<a class="edit" href="/wp-admin/user-edit.php?user_id=<?php echo $agent->ID; ?>">Edit Profile</a>
							</div>
							<div>
								<a class="remove" href="/agent-profile/?agent_id=<?php echo $agent->ID; ?>&action=remove" data-agent-id="<?php echo $agent->ID; ?>">Remove</a>
							</div>
						</td>
						<td><?php echo $agent_class->email; ?></td>
						<td><?php echo $agent_class->company_name; ?></td>
						<td>
							<span class="agent-status <?php echo $agent_class->status; ?>"><?php echo $agent_class->status; ?></span>
							<?php if ( 'yes' === $agent_class->vacation_status ): ?>
								<span class="agent-status inactive">On vacation</span>
							<?php endif ?>
						</td>
						<td><?php echo $agent_class->zipcodes; ?></td>
						<td><?php echo $agent_class->render_preferences(); ?></td>
						<td><?php echo $agent_class->render_property_value(); ?></td>
						<td><?php echo $agent_class->language; ?></td>
						<td><?php echo ucfirst( $agent_class->is_terms_accepted ); ?></td>
					</tr>
				<?php endforeach ?>
			<?php endif ?>
		</tbody>
	</table>

	<div id="delete-agent-popup">
        <div class="popup-wrap">
            <span class="close">X</span>
            <h4>Are you sure you want to Delete account with ID #<span class="agent-id-popup" ></span>?</h4>
            <div class="buttons">
                <a href="#" id="no">Cancel</a>
                <a href="#" id="yes">Yes, I want to totally Delete account from this website</a>
            </div>
        </div>
    </div>

</div>