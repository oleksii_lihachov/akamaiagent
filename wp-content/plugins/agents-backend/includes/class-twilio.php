<?php

namespace Agents_Backend;

use Twilio\Rest\Client;

/**
 * The class containing informatin about the plugin.
 */
class TwilioService
{
    private $account_sid = 'AC41f7c2de20c019e64c279d03d8eabfe4';
    private $auth_token  = '33025eb556666cb2fd2d80030130a96f';

	// A Twilio phone number you purchased at twilio.com/console
    private $from_number = '+18632926329';
	// the number you'd like to send the message to
    private $to_number   = '+18082560245';

    private function send_sms_request( $text ) {

        $client = new Client( $this->account_sid, $this->auth_token );

        $result = $client->messages->create(
            // Where to send a text message (your cell phone?)
            $this->to_number,
            array(
                'from' => $this->from_number,
                'body' => $text
            )
        );

        return $result;
    }

    public function send_sms( $type, $url = '' ) {

        $result = false;
        switch ( $type ) {
            case 'contact':
                $result = $this->send_sms_request( 'Someone has sent a message via the Contact Us page' );
                break;

            case 'agent_request':
                $result = $this->send_sms_request( 'Agent submits an Agent Request - ' . $url );
                break;

            case 'customer_request':
                $result = $this->send_sms_request( 'Customer submits a Customer Request - ' . $url );
                break;
            
            default:
                break;
        }

        return $result;

    }

}
