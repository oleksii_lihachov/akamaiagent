��          �               ,        J     R     d  
   p     {  +   �  
   �  	   �     �  8   �                    &    3  C   8  
   |     �     �     �     �  ,   �     �  	   �       R        h  	   n  
   x  
   �   An email address is the only required field. Company Connection Status DEACTIVATED Disconnect Email Address Error: API key does not appear to be valid. First Name Last Name NOT CONNECTED No MailChimp account found. Check the API key correctly. Other Phone VERIFIED Your Message PO-Revision-Date: 2018-03-10 21:54:36+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.1
Language: es
Project-Id-Version: VFB Pro - Notifications
 Una dirección de correo electrónico es el único campo requerido. Compañía Estado de la conexión DESACTIVADO Desconectar Dirección de email Error: la clave de la API no parece válida. Nombre Apellidos NO CONECTADO No se encontró ninguna cuenta MailChimp. Compruebe que la clave API sea correcta. Otros Teléfono VERIFICADO Tu mensaje 