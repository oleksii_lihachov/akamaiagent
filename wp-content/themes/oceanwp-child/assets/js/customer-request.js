window.onloadCallback = function() {
    grecaptcha.render('recaptcha', {
      'sitekey' : '6LeCraAUAAAAANITFOkHx1XTWXuy20zLJCGl_J0W'
    });
};


(function($){

	"use strict";

	var questions_type = '';
	var current_questions_type

	// URLS
	var contact_us_url = '/contact-us/';

	// User Answers
	var answers = {
		type: '',
		Seller: {},
		Buyer: {},
		General: {},
		result: 'GN-1'

	};

	// Notices
	var notices = {
		Seller: {
			'SN-1': '<p>We’re sorry we are unable to assist you with your Sale at this time, although we’re always here for you should you need our assistance in the future. If you have any other questions or comments, please feel free to <a class="inline" href="' + contact_us_url + '">Contact us</a>.</p>',
			'SN-2': '<p>Great! We sent you a link to confirm your email address. Once your email has been confirmed, we will reach out to our contacts and refer you a top agent in the area you are planning to sell.</p>',
			'SN-3': '<p>Great! We sent you a link to confirm your email address. Once your email has been confirmed, you will receive top agent profiles shortly, to assist you with your “Purchase”.</p><p>We noticed that you are Selling outside of Hawaii. Unfortunately, we currently provide the Akamai Agent service only within the state of Hawaii, although we have contacts and resources across the country. We would be happy to refer you to a top agent in the area you are planning to sell.</p>',
		},
		Buyer: {
			'BN-1': '<p>We’re sorry we are not able to assist you with your Purchase at this time, although we’re always here for you should you need our assistance in the future. If you have any other questions or comments, please feel free to <a class="inline" href="' + contact_us_url + '">Contact us</a>.</p>',
			'BN-2': '<p>Great! We sent you a link to confirm your email address. Once your email has been confirmed, we will reach out to our contacts and refer you a top agent in the area you are planning to Buy.</p>',
			'BN-3': '<p>Great! We sent you a link to confirm your email address. Once your email has been confirmed, you will receive top agent profiles shortly, to assist you with your “Sale”.</p><p>We noticed that you are planning to Purchase outside of Hawaii. Unfortunately, we currently provide the Akamai Agent service only within the state of Hawaii, although we have contacts and resources across the country. We would be happy to refer you to a top agent in the area you are planning to purchase.</p>'
		},
		General: {
			'GN-1': '<p>Great! We sent you a link to confirm your email address. Once your email has been confirmed you will receive your top agent profiles shortly.</p>',
			'GN-2': '<p>We noticed that you plan to Sell and Buy outside of Hawaii. Unfortunately, we currently provide the Akamai Agent service only within the state of Hawaii, although we have contacts and resources across the country. We would be happy to refer you to a top agent in each of the areas you plan to Sell and Buy.</p>',
			'GN-3': '<p>Great! We sent you a link to confirm your email address. Once your email has been confirmed, we will reach out to our contacts and refer you a top agent for each of the areas you are planning to Sell and Buy.</p>',
			'GN-4': '<p>I’m sorry we couldn’t help you with your Sale at this time, although we’re always here for you should you should you need our assistance in the future.</p><p>Your Buyer confirmation email has been sent. Once your email has been confirmed, you will receive your Hawaii “Buyers” agent profiles shortly.</p>',
			'GN-5': '<p>I’m sorry we couldn’t help you with your Purchase at this time, although we’re always here for you should you need our assistance in the future.</p><p>Your Seller confirmation email has been sent. Once your email has been confirmed, you will receive your Hawaii “Sales” agent profiles shortly.</p>',
			'GN-6': '<p>We’re sorry we were not able to assist you with your Sale and Purchase at this time, although we’re always here for you should you need our assistance in the future. If you have any other questions or comments, please feel free to <a class="inline" href="' + contact_us_url + '">Contact us</a>.</p>'
		}
	}

	// Hawaii zip codes
	var hawaii_zipcodes = [
		96701, 96703, 96704, 96705, 96706, 96707, 96708, 96709, 96710, 96712,
		96713, 96714, 96715, 96716, 96717, 96718, 96719, 96720, 96721, 96722,
		96725, 96726, 96727, 96728, 96729, 96730, 96731, 96732, 96733, 96734,
		96737, 96738, 96739, 96740, 96741, 96742, 96743, 96744, 96745, 96746,
		96747, 96748, 96749, 96750, 96751, 96752, 96753, 96754, 96755, 96756,
		96757, 96759, 96760, 96761, 96762, 96763, 96764, 96765, 96766, 96767,
		96768, 96769, 96770, 96771, 96772, 96773, 96774, 96776, 96777, 96778,
		96779, 96780, 96781, 96782, 96783, 96784, 96785, 96786, 96788, 96789,
		96790, 96791, 96792, 96793, 96795, 96796, 96797, 96801, 96802, 96803,
		96804, 96805, 96806, 96807, 96808, 96809, 96810, 96811,	96812, 96813,
		96814, 96815, 96816, 96817, 96818, 96819, 96820, 96821, 96822, 96823,
		96824, 96825, 96826, 96827, 96828, 96830, 96835, 96836, 96837, 96838,
		96839, 96840, 96841, 96843, 96844, 96846, 96847, 96848, 96849, 96850,
		96853, 96854, 96857, 96858, 96859, 96860, 96861, 96863, 96898
	];

	// Process Steps
	var process_language = '';

	jQuery(document).ready(function($) {

		/*========== Customer request form ========== */
		// Request Type
		$('#customer_request_form select[name="customer_request_property_address_state"] option').first().text('State');

		$('#customer_request_form #customer_type div').click(function(event) {
			var type_section = $(this).closest('#customer_type');
			questions_type = $(this).data('type');

			answers.type = questions_type;

			changeCustomersFormData( questions_type );

			// Set Progress
			$('.request_form_progress div[data-step="customer_type"]').addClass('active');
			$('.request_form_progress div[data-step="customer_type"] span').text( $(this).data('step') );

			type_section.addClass('animated fadeOutUp');

			// Add in animation for next question
			setTimeout(function(){
				// Hide previous question
				type_section.addClass('invisible');

				$('#customer_request_form #questions > div.customer_request_property_type').addClass('animated fadeInUp active').removeClass('d-none');

			}, 500);
		});


		// Select Answer
		$('#customer_request_form .wpcf7-list-item').click(function(event) {
			selectAnswer( $(this) );
		});

		$('#customer_request_form .wpcf7-select').on('change', function (event) {
			selectAnswer( $(this) );
		});

		// Change language in the progress box
		$('.customer_request_other_language select').change(function(event) {
			$('.request_form_progress div[data-step="customer_request_need_language"] span').text( $(this).val() );
		});


		// Click to next question button
		$('#customer_request_form a.btn').click(function(event) {

			var current_question = $(this).closest('.question');

			nextQuestion( current_question );

		});

		$(document).on( 'click', '.request_form_progress > div.active', function() {
			if ($(this).hasClass('disabled')) {
				return true;
			}

			var step = $(this).data('step');
			resetProgress( step );
		});

		// Add Select value element
		$('#customer_request_form select').after( "<span class='select-value'></span>" );
		$('#customer_request_form select').each(function(index, el) {
			const selectField = $(this);
			let value = $(this).find('option:selected').html();

			$(this).next().text(value);

			$(this).next().click((event) => {
				console.log('Ok');
				selectField.trigger('click');
			});

		});
		$('#customer_request_form select').change(function(event) {
			let value = $(this).find('option:selected').html();

			$(this).next().text(value);
		});

	});

	function changeCustomersFormData( type ){

		switch( type ){
			case 'Seller':
				current_questions_type = 'Seller';

				$('#customer_request_form .customer_request_property_type h3').text('What type of property would you like to sell?');
				$('#customer_request_form .customer_request_price_range h3').text('Roughly, how much is your property worth?');
				$('#customer_request_form .customer_request_how_soon h3').text('How soon do you plan to sell? ');
				$('#customer_request_form .customer_request_outside_of_hawaii h3').text('Oops, we noticed you entered an address outside of Hawaii. We currently provide the Akamai Agent service only within the state of Hawaii, although we have contacts and resources across the country. We would be happy to reach out to our trusted contacts and refer you to a top agent in the area you are planning to sell.');

				$('.question.customer_request_how_soon').data('next', 'customer_request_need_language');
				$('.question.customer_request_need_language').data('next', 'customer_request_property_address_seller');

				$('.request_form_progress div[data-step="customer_request_price_range"]').html('Estimated Value<br><span></span>');
				break;

			case 'Buyer':
				current_questions_type = 'Buyer';

				$('#customer_request_form .customer_request_property_type h3').text('What type of property would you like to Buy?');
				$('#customer_request_form .customer_request_price_range h3').text('What is your estimated budget?');
				$('#customer_request_form .customer_request_how_soon h3').text('When will you be ready to buy?');
				$('#customer_request_form .customer_request_outside_of_hawaii h3').text('Oops, we noticed you entered a zip code outside of Hawaii. We currently provide the Akamai Agent service only within the state of Hawaii, although we have contacts and resources across the country. We would be happy to reach out to our trusted contacts and refer you to a top agent in the area you are planning to buy.');

				$('.question.customer_request_how_soon').data('next', 'customer_request_preapproved_by_lender');
				$('.question.customer_request_need_language').data('next', 'customer_request_property_address_buyer');
				// $('.question.customer_request_preapproved_by_lender').data('next', 'customer_request_property_address_buyer');

				$('.request_form_progress div[data-step="customer_request_price_range"]').html('Estimated Budget<br><span></span>');
				break;

			case 'Seller & Buyer':
				current_questions_type = 'Seller';

				$('#customer_request_form .customer_request_property_type h3').text('What type of property would you like to sell?');
				$('#customer_request_form .customer_request_price_range h3').text('Roughly, how much is your property worth?');
				$('#customer_request_form .customer_request_how_soon h3').text('How soon do you plan to sell? ');
				$('#customer_request_form .customer_request_outside_of_hawaii h3').text('Oops, we noticed you entered an address outside of Hawaii. We currently provide the Akamai Agent service only within the state of Hawaii, although we have contacts and resources across the country. We would be happy to reach out to our trusted contacts and refer you to a top agent in the area you are planning to sell.');

				$('.question.customer_request_how_soon').data('next', 'customer_request_need_language');
				$('.question.customer_request_need_language').data('next', 'customer_request_property_address_seller');
				$('.customer_request_property_address_seller').data('next', 'customer_request_property_type');

				$('.request_form_progress div[data-step="customer_request_price_range"]').html('Estimated Value<br><span></span>');
				break;
		}
	}

	function selectAnswer( answer ) {
		var isSelect = answer.hasClass('wpcf7-select');
		var value = '';
		var fieldName = '';

		answer.removeClass('active error');

		if (isSelect) {
			value = answer.val();
			fieldName = answer.attr('name');
		} else {
			var $input = answer.find('input');

			value = $input.val();
			fieldName = $input.attr('name');
		}

		$('select[name="' + fieldName + '"]').val(value);
		$('input[name="' + fieldName + '"][value="' + value + '"]').prop("checked", true);

		$('#recaptcha').removeClass('error');

		answer.addClass('active');

		// Languages question
		if ( answer.closest('.question').hasClass('customer_request_need_language') ) {
			if ( ! answer.find('input[value="No"]').is(':checked') ) {
				answer.closest('.question').find('.customer_request_other_language select').removeClass('d-none');
				return;
			} else {
				answer.closest('.question').find('.customer_request_other_language select').addClass('d-none');
			}
		}

		// Set Progress
		$('.request_form_progress div[data-step="' + answer.closest('.question').data('step') + '"]').addClass('active');
		$('.request_form_progress div[data-step="' + answer.closest('.question').data('step') + '"] span').text( value );
	
		
		var current_question = answer.closest('.question');
		nextQuestion( current_question );
	}

	function nextQuestion( question ) {

		if ( ! validateAnswer( question ) ) {
			return false;
		}

		var next_question = question.data('next');

		// Hide progress after language
		if ( next_question === 'customer_request_property_address_seller' || next_question === 'customer_request_property_address_buyer' ) {

			process_language = $('.request_form_progress div[data-step="customer_request_need_language"] span').text();

			resetProgress();
			$('.request_form_progress').addClass('d-none');
		}

		// Save user answer to object
		saveAnswer( question );

		// Move from seller to buyer
		if ( next_question === 'customer_request_property_type' && questions_type === 'Seller & Buyer' ) {

			resetQuestions( 'Buyer' );

			$('.question.customer_request_preapproved_by_lender').data('next', 'customer_request_property_address_buyer');

			$('.request_form_progress div[data-step="customer_type"]').addClass('active disabled');
			$('.request_form_progress div[data-step="customer_type"] span').text( 'Buy' );
			$('.request_form_progress div[data-step="customer_request_need_language"]').addClass('active disabled');
			$('.request_form_progress div[data-step="customer_request_need_language"] span').text( process_language );
			$('.request_form_progress').removeClass('d-none');
		}

		showNotice( question );

		// Add out animation
		question.addClass('animated fadeOutUp');

		// Add in animation for next question
		setTimeout(function(){
			// Hide previous question
			question.addClass('invisible').removeClass('active');

			$( '.question.' + next_question ).addClass('animated fadeInUp active').removeClass('d-none');
		}, 500);

		// Save Request
		if ( next_question === 'customer_request_notice' ) {
			saveRequest();
		}
	}

	function validateAnswer( question ) {

		var valid = true;

		question.find('.wpcf7-list-item').removeClass('error');
		question.find('input, select').removeClass('error');


		if ( question.find('input[type="radio"]').length > 0 && question.find('input[type="radio"]:checked').length == 0 ) {

			question.find('.wpcf7-list-item').addClass('error');
			valid = false;
		}

		if ( question.find('input[type="checkbox"]').length > 0 && question.find('input[type="checkbox"]:checked').length == 0 ) {

			question.find('.wpcf7-list-item').addClass('error');
			valid = false;
		}

		question.find('select').each(function(index, el) {
			if ( ( $(this).val() === '' || $(this).val() === 'undefined' ) && $(this).is(':visible') ) {
				question.find('select').addClass('error');
				valid = false;
			}
		});

		if ( question.find('input[type="text"]').length > 0 || question.find('input[type="email"]').length > 0 ) {

			question.find('input').each(function(index, el) {
				if( $(this).val() == '' && $(this).attr('name') !== 'customer_request_property_address_unit' ){
					$(this).addClass('error');
					valid = false;
				}
			});
		}

		// Recaptcha
		if ( question.find('#recaptcha').length > 0 ) {
			if( ! grecaptcha.getResponse() || grecaptcha.getResponse().length == 0 ) {
				question.find('#recaptcha').addClass('error');
				valid = false;
			}
		}

		return valid;
	}

	function saveAnswer( question ){

		var current_step = question.data('step');

		switch(current_step) {
			case 'customer_request_property_address_seller':
				answers[current_questions_type]['customer_request_property_address_street'] = question.find('input[name="customer_request_property_address_street"]').val();
				answers[current_questions_type]['customer_request_property_address_unit'] = question.find('input[name="customer_request_property_address_unit"]').val();
				answers[current_questions_type]['customer_request_property_address_city'] = question.find('input[name="customer_request_property_address_city"]').val();
				answers[current_questions_type]['customer_request_property_address_state'] = question.find('select[name="customer_request_property_address_state"]').val();
				answers[current_questions_type]['customer_request_property_address_zipcode'] = question.find('input[name="customer_request_property_address_zipcode"]').val();
				break;

			case 'customer_request_need_language':
				answers['General'][current_step] = question.find('input:checked').val();

				if ( question.find('select:visible') ) {
					answers['General']['customer_request_other_language'] = question.find('select').val();
				}
				break;

			case 'customer_request_property_address_buyer':
				answers[current_questions_type]['customer_request_customer_zip'] = question.find('input[name="customer_request_customer_zip"]').val();
				break;

			case 'customer_request_customer_name':
				answers['General']['customer_request_customer_first_name'] = question.find('input[name="customer_request_customer_first_name"]').val();
				answers['General']['customer_request_customer_last_name'] = question.find('input[name="customer_request_customer_last_name"]').val();
				answers['General']['customer_request_customer_email'] = question.find('input[name="customer_request_customer_email"]').val();
				break;

			case 'customer_request_outside_of_hawaii':
				answers['General']['customer_request_outside_of_hawaii'] = question.find('input:checked').val();
				break;

			default:
				answers[current_questions_type][current_step] = question.find('input:checked').val();
				break;
		}
	}

	function showNotice( question ) {

		// Show SQ-5.1
		if ( question.data('step') === 'customer_request_property_address_seller' && questions_type === 'Seller' ) {

			if ( question.find('select').val() !== 'Hawaii' ) {
				$('#customer_request_form .customer_request_customer_name').data('next', 'customer_request_outside_of_hawaii');
			}
		}

		// Show BQ-6.1
		if ( question.data('step') === 'customer_request_property_address_buyer' && questions_type === 'Buyer' ) {

			if ( hawaii_zipcodes.indexOf( parseInt( question.find('input').val() ) ) == -1 ) {
				$('#customer_request_form .customer_request_customer_name').data('next', 'customer_request_outside_of_hawaii');
			}
		}

		// Show Sell/Buy BQ-6.1
		if ( question.data('step') === 'customer_request_property_address_buyer' && questions_type === 'Seller & Buyer' && current_questions_type === 'Buyer' ) {

			if ( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) == -1 || hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) == -1 ) {

				$('#customer_request_form .customer_request_customer_name').data('next', 'customer_request_outside_of_hawaii');

				if ( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) == -1 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) == -1 ) {
					$('#customer_request_form .customer_request_outside_of_hawaii h3').html( notices.General['GN-2'] );
					answers.result = 'GN-2';
				} else if( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) >= 0 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) == -1 ){
					$('#customer_request_form .customer_request_outside_of_hawaii h3').html( notices.Seller['SN-3'] );
					answers.result = 'SN-3';
				} else if( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) == -1 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) >= 0 ){
					$('#customer_request_form .customer_request_outside_of_hawaii h3').html( notices.Buyer['BN-3'] );
					answers.result = 'BN-3';
				}
			}

		}

		// SQ-5.1 || BQ-6.1 result
		if ( question.data('step') === 'customer_request_outside_of_hawaii' ) {

			$('#customer_request_form .customer_request_outside_of_hawaii').data('next', 'customer_request_notice');

			if ( questions_type === 'Seller' ) {
				if ( question.find('input:checked').val() === 'No thanks' ) {
					$('#customer_request_form .customer_request_notice h3').html(notices.Seller['SN-1']);
					answers.result = 'SN-1';
				} else {
					$('#customer_request_form .customer_request_notice h3').html(notices.Seller['SN-2']);
					answers.result = 'SN-2';
				}
			}

			if ( questions_type === 'Buyer' ) {

				if ( question.find('input:checked').val() === 'No thanks' ) {
					$('#customer_request_form .customer_request_notice h3').html(notices.Buyer['BN-1']);
					answers.result = 'BN-1';
				} else {
					$('#customer_request_form .customer_request_notice h3').html(notices.Buyer['BN-2']);
					answers.result = 'BN-2';
				}
			}

			if ( questions_type === 'Seller & Buyer' ) {

				if ( question.find('input:checked').val() === 'No thanks' ) {

					if ( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) == -1 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) == -1 ) {
						$('#customer_request_form .customer_request_notice h3').html(notices.General['GN-6']);
						answers.result = 'GN-6';
					} else if( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) >= 0 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) == -1 ){
						$('#customer_request_form .customer_request_notice h3').html(notices.General['GN-4']);
						answers.result = 'GN-4';
						answers.type = 'Buyer';
					} else if( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) == -1 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) >= 0 ){
						$('#customer_request_form .customer_request_notice h3').html(notices.General['GN-5']);
						answers.result = 'GN-5';
						answers.type = 'Seller';
					}

				} else {

					if ( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) == -1 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) == -1 ) {
						$('#customer_request_form .customer_request_notice h3').html(notices.Buyer['GN-3']);
						answers.result = 'GN-3';
					} else if( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) >= 0 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) == -1 ){
						$('#customer_request_form .customer_request_notice h3').html(notices.Buyer['SN-2']);
						answers.result = 'SN-2';
					} else if( hawaii_zipcodes.indexOf( parseInt( answers.Buyer.customer_request_customer_zip ) ) == -1 && hawaii_zipcodes.indexOf( parseInt( answers.Seller.customer_request_property_address_zipcode ) ) >= 0 ){
						$('#customer_request_form .customer_request_notice h3').html(notices.Buyer['BN-2']);
						answers.result = 'BN-2';
					}
				}
			}
		}

	}

	function resetQuestions( type ){

		$('#customer_request_form .question').removeClass('invisible animated fadeInUp fadeOutUp active').addClass('d-none');
		$('#customer_request_form .question input').prop("checked", false);
		$('#customer_request_form .wpcf7-select').val();
		$('#customer_request_form span.wpcf7-list-item.active').removeClass('active');

		$('.request_form_progress div.active').removeClass('active');
		$('.request_form_progress div span').text('');

		changeCustomersFormData( type );

	}

	function resetProgress( step = null ){

		if ( step === 'customer_type' ) {
			$('#customer_request_form #customer_type').removeClass('invisible fadeOutUp').addClass('animated fadeInUp');
			$('#customer_request_form .question').removeClass('invisible animated fadeInUp fadeOutUp active').addClass('d-none');
			$('#customer_request_form .question input').prop("checked", false);
			$('#customer_request_form span.wpcf7-list-item.active').removeClass('active');

			$('.request_form_progress div.active').removeClass('active');
			$('.request_form_progress div span').text('');
		} else if ( step !== null ) {
			var active_question_index = $('#customer_request_form .question.' + step).index();

			$('#customer_request_form .question').each(function(index, el) {
				if ( index > active_question_index ) {
					$(this).removeClass('invisible animated fadeInUp fadeOutUp active').addClass('d-none');
					$(this).find('input').prop("checked", false);
					$(this).find('.wpcf7-list-item.active').removeClass('active');

					$('.request_form_progress div[data-step="' + $(this).data('step') + '"]').removeClass('active');
					$('.request_form_progress div[data-step="' + $(this).data('step') + '"] span').text('');
				}
			});

			$('#customer_request_form .question.' + step).removeClass('invisible fadeOutUp').addClass('fadeInUp');
		} else {

			$('.request_form_progress div.active').removeClass('active');
			$('.request_form_progress div span').text('');

		}

	}


	function saveRequest() {
		if (answers.result === 'SN-1' || answers.result === 'BN-1' || answers.result === 'GN-6') {
			return false;
		}

		$.post(
			ajax_customer_request.ajax_url,
			{
				action: 'save_customer_request',
				answers: answers,
				nonce: ajax_customer_request.ajax_nonce
			},
			function (data, textStatus, xhr) {
				console.log(data);
			})
	}

})(jQuery)